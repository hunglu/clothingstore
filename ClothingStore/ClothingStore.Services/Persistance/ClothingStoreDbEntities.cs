﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using ClothingStore.Services.Entities;

namespace ClothingStore.Services.Persistance
{
    public class ClothingStoreDbEntities: DbContext
    {
        public ClothingStoreDbEntities() : base("name=ClothingStoreDbEntities")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Item>()
                .HasMany<ItemSize>(i => i.Sizes)
                .WithMany(c => c.Items)
                .Map(cs =>
                {
                    cs.MapLeftKey("ItemId");
                    cs.MapRightKey("ItemSizeId");
                    cs.ToTable("ItemItemSize");
                });

            modelBuilder.Entity<Item>()
                .HasMany<ItemColor>(i => i.Colors)
                .WithMany(c => c.Items)
                .Map(cs =>
                {
                    cs.MapLeftKey("ItemId");
                    cs.MapRightKey("ItemColorId");
                    cs.ToTable("ItemItemColor");
                });
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Item> Items { get; set; }

        public DbSet<ItemSize> ItemSizes { get; set; }
        public DbSet<ItemColor> ItemColors { get; set; }
        public DbSet<Storage> Storages { get; set; }
        public DbSet<StorageDetail> StorageDetails { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<Sale> Sales { get; set; }
        
    }
}
