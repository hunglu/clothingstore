﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothingStore.Services.Entities
{
    public class ItemSize: BaseEntity
    {
        public ItemSize() {
            Items = new List<Item>();
        }

        public string SizeCode { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}
