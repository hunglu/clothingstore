﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClothingStore.Services.Entities
{
    public class StorageDetail: BaseEntityWithTracking
    {
        [Key]
        [Column(Order = 2)]
        public Guid StorageId { get; set; }
        public Guid OrderDetailId { get; set; }
        public decimal PriceAtStore { get; set; }
        public int Quantity { get; set; }

        public virtual Storage Storage { get; set; }
        public virtual OrderDetail OrderDetail { get; set; }
    }
}
