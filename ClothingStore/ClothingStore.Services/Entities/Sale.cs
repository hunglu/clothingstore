﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothingStore.Services.Entities
{
    public class Sale: BaseEntityWithTracking
    {
        public Guid ItemId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public decimal PriceAtRetail { get; set; }

        public virtual Item Item { get; set; }
    }
}
