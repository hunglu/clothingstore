﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClothingStore.Services.Entities
{
    public class BaseEntity
    {
        [Key]
        [Column(Order = 1)]
        public Guid Id { get; set; }
    }
}
