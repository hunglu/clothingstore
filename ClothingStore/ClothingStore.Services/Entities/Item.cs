﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothingStore.Services.Entities
{
    public class Item : BaseEntityWithTracking
    {
        public Item() {
            Sizes = new List<ItemSize>();
            Colors = new List<ItemColor>();
        }

        public string Name { get; set; }
        public string Code { get; set; }

        public virtual ICollection<ItemSize> Sizes { get; set; }
        public virtual ICollection<ItemColor> Colors { get; set; }

        public Guid CategoryId { get; set; }
        public virtual Category Category { get; set; }
    }
}
