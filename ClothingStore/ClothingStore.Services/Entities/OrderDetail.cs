﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClothingStore.Services.Entities
{
    public class OrderDetail: BaseEntityWithTracking
    {
        [Key]
        [Column(Order = 2)]
        public Guid OrderId { get; set; }
        public Guid ItemId { get; set; }
        public Guid SupplierId { get; set; }
        public int Quantity { get; set; }
        public decimal PriceAtStore { get; set; }

        public virtual Order Order { get; set; }
        public virtual Supplier Supplier { get; set; }
        public virtual Item Item { get; set; }
    }
}
