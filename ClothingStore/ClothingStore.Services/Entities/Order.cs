﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothingStore.Services.Entities
{
    public class Order : BaseEntityWithTracking
    {
        public DateTime OrderDate { get; set; }
        public decimal TotalPrice { get; set; }
    }
}
