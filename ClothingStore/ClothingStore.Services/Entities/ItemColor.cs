﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothingStore.Services.Entities
{
    public class ItemColor : BaseEntity
    {
        public ItemColor()
        {
            Items = new List<Item>();
        }

        public string ColorCode { get; set; }

        public virtual ICollection<Item> Items { get; set; }
    }
}
