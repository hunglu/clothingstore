﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothingStore.Services.Entities
{
    public class Storage: BaseEntityWithTracking
    {
        public Guid ItemId { get; set; }
        public int Quantity { get; set; }

        public virtual Item Item { get; set; }
    }
}
