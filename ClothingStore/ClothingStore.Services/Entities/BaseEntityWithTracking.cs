﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClothingStore.Services.Entities
{
    public class BaseEntityWithTracking : BaseEntity
    {
        public DateTime? UpdatedDateTime { get; set; }
        public Guid? UpdatedBy { get; set; }
        public DateTime? CreatededDateTime { get; set; }
        public Guid? CreatedBy { get; set; }

    }
}
