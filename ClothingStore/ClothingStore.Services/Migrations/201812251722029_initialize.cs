namespace ClothingStore.Services.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initialize : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        CategoryName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ItemColors",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ColorCode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Items",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Code = c.String(),
                        CategoryId = c.Guid(nullable: false),
                        UpdatedDateTime = c.DateTime(),
                        UpdatedBy = c.Guid(),
                        CreatededDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.ItemSizes",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        SizeCode = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.OrderDetails",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OrderId = c.Guid(nullable: false),
                        ItemId = c.Guid(nullable: false),
                        SupplierId = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        PriceAtStore = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UpdatedDateTime = c.DateTime(),
                        UpdatedBy = c.Guid(),
                        CreatededDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.Id, t.OrderId })
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .ForeignKey("dbo.Suppliers", t => t.SupplierId, cascadeDelete: true)
                .Index(t => t.OrderId)
                .Index(t => t.ItemId)
                .Index(t => t.SupplierId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OrderDate = c.DateTime(nullable: false),
                        TotalPrice = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UpdatedDateTime = c.DateTime(),
                        UpdatedBy = c.Guid(),
                        CreatededDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Suppliers",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Name = c.String(),
                        Address = c.String(),
                        Phone = c.String(),
                        UpdatedDateTime = c.DateTime(),
                        UpdatedBy = c.Guid(),
                        CreatededDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Sales",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ItemId = c.Guid(nullable: false),
                        From = c.DateTime(nullable: false),
                        To = c.DateTime(nullable: false),
                        PriceAtRetail = c.Decimal(nullable: false, precision: 18, scale: 2),
                        UpdatedDateTime = c.DateTime(),
                        UpdatedBy = c.Guid(),
                        CreatededDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .Index(t => t.ItemId);
            
            CreateTable(
                "dbo.StorageDetails",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        StorageId = c.Guid(nullable: false),
                        OrderDetailId = c.Guid(nullable: false),
                        PriceAtStore = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Quantity = c.Int(nullable: false),
                        UpdatedDateTime = c.DateTime(),
                        UpdatedBy = c.Guid(),
                        CreatededDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                        OrderDetail_Id = c.Guid(),
                        OrderDetail_OrderId = c.Guid(),
                    })
                .PrimaryKey(t => new { t.Id, t.StorageId })
                .ForeignKey("dbo.OrderDetails", t => new { t.OrderDetail_Id, t.OrderDetail_OrderId })
                .ForeignKey("dbo.Storages", t => t.StorageId, cascadeDelete: true)
                .Index(t => t.StorageId)
                .Index(t => new { t.OrderDetail_Id, t.OrderDetail_OrderId });
            
            CreateTable(
                "dbo.Storages",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        ItemId = c.Guid(nullable: false),
                        Quantity = c.Int(nullable: false),
                        UpdatedDateTime = c.DateTime(),
                        UpdatedBy = c.Guid(),
                        CreatededDateTime = c.DateTime(nullable: false),
                        CreatedBy = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .Index(t => t.ItemId);
            
            CreateTable(
                "dbo.ItemItemColor",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                        ItemColorId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ItemId, t.ItemColorId })
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("dbo.ItemColors", t => t.ItemColorId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.ItemColorId);
            
            CreateTable(
                "dbo.ItemItemSize",
                c => new
                    {
                        ItemId = c.Guid(nullable: false),
                        ItemSizeId = c.Guid(nullable: false),
                    })
                .PrimaryKey(t => new { t.ItemId, t.ItemSizeId })
                .ForeignKey("dbo.Items", t => t.ItemId, cascadeDelete: true)
                .ForeignKey("dbo.ItemSizes", t => t.ItemSizeId, cascadeDelete: true)
                .Index(t => t.ItemId)
                .Index(t => t.ItemSizeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StorageDetails", "StorageId", "dbo.Storages");
            DropForeignKey("dbo.Storages", "ItemId", "dbo.Items");
            DropForeignKey("dbo.StorageDetails", new[] { "OrderDetail_Id", "OrderDetail_OrderId" }, "dbo.OrderDetails");
            DropForeignKey("dbo.Sales", "ItemId", "dbo.Items");
            DropForeignKey("dbo.OrderDetails", "SupplierId", "dbo.Suppliers");
            DropForeignKey("dbo.OrderDetails", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.OrderDetails", "ItemId", "dbo.Items");
            DropForeignKey("dbo.ItemItemSize", "ItemSizeId", "dbo.ItemSizes");
            DropForeignKey("dbo.ItemItemSize", "ItemId", "dbo.Items");
            DropForeignKey("dbo.ItemItemColor", "ItemColorId", "dbo.ItemColors");
            DropForeignKey("dbo.ItemItemColor", "ItemId", "dbo.Items");
            DropForeignKey("dbo.Items", "CategoryId", "dbo.Categories");
            DropIndex("dbo.ItemItemSize", new[] { "ItemSizeId" });
            DropIndex("dbo.ItemItemSize", new[] { "ItemId" });
            DropIndex("dbo.ItemItemColor", new[] { "ItemColorId" });
            DropIndex("dbo.ItemItemColor", new[] { "ItemId" });
            DropIndex("dbo.Storages", new[] { "ItemId" });
            DropIndex("dbo.StorageDetails", new[] { "OrderDetail_Id", "OrderDetail_OrderId" });
            DropIndex("dbo.StorageDetails", new[] { "StorageId" });
            DropIndex("dbo.Sales", new[] { "ItemId" });
            DropIndex("dbo.OrderDetails", new[] { "SupplierId" });
            DropIndex("dbo.OrderDetails", new[] { "ItemId" });
            DropIndex("dbo.OrderDetails", new[] { "OrderId" });
            DropIndex("dbo.Items", new[] { "CategoryId" });
            DropTable("dbo.ItemItemSize");
            DropTable("dbo.ItemItemColor");
            DropTable("dbo.Storages");
            DropTable("dbo.StorageDetails");
            DropTable("dbo.Sales");
            DropTable("dbo.Suppliers");
            DropTable("dbo.Orders");
            DropTable("dbo.OrderDetails");
            DropTable("dbo.ItemSizes");
            DropTable("dbo.Items");
            DropTable("dbo.ItemColors");
            DropTable("dbo.Categories");
        }
    }
}
