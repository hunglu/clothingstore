namespace ClothingStore.Services.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ClothingStore.Services.Persistance.ClothingStoreDbEntities>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ClothingStore.Services.Persistance.ClothingStoreDbEntities context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            
            //context.Categories.AddOrUpdate(new Entities.Category { CategoryName = "Shirt" });
            //var category = context.Categories.FirstOrDefault(x => x.CategoryName == "Shirt");
            //context.Items.AddOrUpdate(new Entities.Item { CategoryId = category.Id, Code= "TShirt", Name= "T-Shirt" , CreatededDateTime = DateTime.Now });
            //context.Items.AddOrUpdate(new Entities.Item { CategoryId = category.Id, Code = "DressShirt", Name = "Dress Shirt", CreatededDateTime = DateTime.Now });

            //base.Seed(context);
        }
    }
}
